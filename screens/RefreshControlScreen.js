import React, { Component } from 'react';
import { View, ScrollView, RefreshControl, StyleSheet } from 'react-native';

export default class RefreshControlScreen extends Component {
  
  state={ refreshing: false }

  _onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => this.setState({refreshing: false }), 2000);
  }
  
  render() {
    return <ScrollView 
    refreshControl={<RefreshControl 
      refreshing={this.state.refreshing}
      onRefresh={this._onRefresh}
    />}
    style={{marginTop:30}}>
        <View style={{backgroundColor: 'green', height: 30}} />
    </ScrollView>;
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});