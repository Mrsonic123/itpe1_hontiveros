import React from "react";
import { StyleSheet, View, Text, Slider, } from "react-native";

export default class SliderExample extends React.Component {
    state = {
        value: 10
    };

    render() {
        return (
            <View style={styles.container}>
                <Slider
                    value={this.state.value}
                    onValueChange={value => this.setState({ value })}
                />
                <Text>
                    Value: {this.state.value}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        alignItems: "stretch",
        justifyContent: "center"
    }
});